#include "render.h"
#include "opengl_utils.h"
#include "resource_types.h"
#include "utils.h"

#include <cstdio>
#include <cstring>
#include <cstdlib>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// NOT ALLOWED FUNCTION POINTERS TO CODE IN GAME DLL
internal void PrintError( const char *msg )
{
  fprintf( stderr, "%s\n", msg );
}

internal Shader GetUniformLocations( uint32_t program )
{
  Shader result = {};
  result.program = program;
  result.colourLocation = glGetUniformLocation( result.program, "colour" );
  result.combinedMatrixLocation =
    glGetUniformLocation( result.program, "combinedMatrix" );
  result.diffuseTextureLocation =
    glGetUniformLocation( result.program, "diffuseTexture" );
  result.positionsTextureLocation =
    glGetUniformLocation( result.program, "positionsTexture" );
  result.normalsTextureLocation =
    glGetUniformLocation( result.program, "normalsTexture" );
  result.depthTextureLocation =
    glGetUniformLocation( result.program, "depthTexture" );
  result.outputTypeLocation =
    glGetUniformLocation( result.program, "outputType" );
  result.modelMatrixLocation =
    glGetUniformLocation( result.program, "modelMatrix" );
  result.combinedMatrixLocation =
    glGetUniformLocation( result.program, "combinedMatrix" );
  result.numSamplesLocation =
    glGetUniformLocation( result.program, "numSamples" );
  result.shadowTextureLocation =
    glGetUniformLocation( result.program, "shadowTexture" );
  result.shadowViewProjectionLocation =
    glGetUniformLocation( result.program, "shadowViewProjection" );
  result.windowDimensionsLocation =
    glGetUniformLocation( result.program, "windowDimensions" );
  result.invViewProjectionLocation =
    glGetUniformLocation( result.program, "invViewProjection" );
  result.invModelMatrixLocation =
    glGetUniformLocation( result.program, "invModelMatrix" );
  return result;
}

Shader CreateShader( const char *vertex, const char *fragment )
{
  Shader result = {};
  uint32_t program = OpenGLCreateShader( vertex, strlen( vertex ), fragment,
                                         strlen( fragment ), PrintError );
  if ( program )
  {
    result = GetUniformLocations( program );
  }
  else
  {
    OpenGLDeleteShader( program );
  }
  return result;
}

struct ReadFileResult
{
  void *memory;
  uint32_t size;
};

internal uint32_t GetFileSize( FILE *file )
{
  fseek( file, 0, SEEK_END );
  uint32_t result = ftell( file );
  rewind( file );
  return result;
}

internal ReadFileResult ReadEntireFile( const char *path )
{
  ReadFileResult result = {};
  FILE *file = fopen( path, "rb" );
  if ( file )
  {
    result.size = GetFileSize( file );
    result.memory = malloc( result.size );
    if ( result.memory )
    {
      if ( fread( result.memory, 1, result.size, file ) != result.size )
      {
        free( result.memory );
        result.memory = nullptr;
        result.size = 0;
      }
    }
    else
    {
      result.size = 0;
    }
    fclose( file );
  }
  return result;
}

internal void FreeFileMemory( void *data )
{
  free( data );
}

Shader LoadShader( const char *path )
{
  Shader result = {};
  ReadFileResult fileData = ReadEntireFile( path );
  if ( fileData.memory )
  {
    ShaderFileHeader *header = (ShaderFileHeader *)fileData.memory;
    const char *vertexSource =
      (const char *)fileData.memory + header->vertexShaderOffset;
    const char *fragmentSource =
      (const char *)fileData.memory + header->fragmentShaderOffset;

    uint32_t program = OpenGLCreateShader(
      vertexSource, header->vertexShaderLength, fragmentSource,
      header->fragmentShaderLength, PrintError );

    FreeFileMemory( fileData.memory );

    if ( program )
    {
      result = GetUniformLocations( program );
    }
    else
    {
      OpenGLDeleteShader( program );
    }
  }
  return result;
}

// TODO: Load all submeshes.
OpenGLStaticMesh LoadMesh( const char *path )
{
  OpenGLStaticMesh result = {};
  ReadFileResult fileData = ReadEntireFile( path );
  if ( fileData.memory )
  {
    MeshFileHeader* header = (MeshFileHeader*)fileData.memory;
    SubMeshHeader *subMeshHeaders = (SubMeshHeader*)( header + 1 );

    uint32_t numIndices = subMeshHeaders[0].numIndices;
    uint32_t numVertices = subMeshHeaders[0].numVertices;
    void *vertices = (char *)fileData.memory + subMeshHeaders[0].verticesOffset;
    uint32_t *indices = (uint32_t *)( (char *)fileData.memory +
                                      subMeshHeaders[0].indicesOffset );

    // NOTE: Don't forget to update the size of this array.
    OpenGLVertexAttribute attribs[3];
    ASSERT( subMeshHeaders[0].vertexDataFlags & VERTEX_DATA_POSITION );
    attribs[0].index = VERTEX_ATTRIBUTE_POSITION;
    attribs[0].numComponents = 3;
    attribs[0].componentType = GL_FLOAT;
    attribs[0].normalized = GL_FALSE;
    attribs[0].offset = 0;
    uint32_t i = 1;
    uint32_t vertexSize = 12;
    if ( subMeshHeaders[0].vertexDataFlags & VERTEX_DATA_NORMAL )
    {
      attribs[i].index = VERTEX_ATTRIBUTE_NORMAL;
      attribs[i].numComponents = 3;
      attribs[i].componentType = GL_FLOAT;
      attribs[i].normalized = GL_FALSE;
      attribs[i].offset = sizeof( float ) * 3;
      vertexSize += 12;
      i++;
    }
    if ( subMeshHeaders[0].vertexDataFlags & VERTEX_DATA_TEXTURE_COORDINATE )
    {
      attribs[i].index = VERTEX_ATTRIBUTE_TEXTURE_COORDINATE;
      attribs[i].numComponents = 2;
      attribs[i].componentType = GL_FLOAT;
      attribs[i].normalized = GL_FALSE;
      attribs[i].offset = sizeof( float ) * 6;
      vertexSize += 8;
      i++;
    }

    result = OpenGLCreateStaticMesh( vertices, numVertices, indices, numIndices,
                                     vertexSize, attribs, i, GL_TRIANGLES );

    FreeFileMemory( fileData.memory );
  }
  return result;
}

uint32_t LoadTexture( const char *path )
{
  uint32_t result = 0;
  ReadFileResult fileData = ReadEntireFile( path );
  if ( fileData.memory )
  {
    TextureFileHeader *header = (TextureFileHeader*)fileData.memory;
    result = OpenGLCreateTexture(
      header->width, header->height, header->pixelFormat,
      header->pixelComponentType,
      (uint8_t *)fileData.memory + sizeof( TextureFileHeader ) );

    FreeFileMemory( fileData.memory );
  }
  return result;
}

enum
{
	RENDER_OUTPUT_UNKNOWN = 0,
	RENDER_OUTPUT_DIFFUSE_COLOUR = 1,
	RENDER_OUTPUT_POSITIONS = 2,
	RENDER_OUTPUT_NORMALS = 3,
	RENDER_OUTPUT_SHADED = 4,
  RENDER_OUTPUT_DEPTH = 5,
};

void InitializeGBuffer( GBuffer* gbuffer, uint32_t width, uint32_t height )
{
  gbuffer->width = width;
  gbuffer->height = height;
  gbuffer->msaa = 1;
  glGenFramebuffers( 1, &gbuffer->fbo );
  glGenRenderbuffers( 1, &gbuffer->diffuseBuffer );
  glGenRenderbuffers( 1, &gbuffer->depthBuffer );
  glGenRenderbuffers( 1, &gbuffer->positionsBuffer );
  glGenRenderbuffers( 1, &gbuffer->normalsBuffer );

  glBindFramebuffer( GL_FRAMEBUFFER, gbuffer->fbo );

  glBindRenderbuffer( GL_RENDERBUFFER, gbuffer->diffuseBuffer );
  glRenderbufferStorageMultisample( GL_RENDERBUFFER, gbuffer->msaa, GL_RGB,
                                    gbuffer->width, gbuffer->height );
  glFramebufferRenderbuffer( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                             GL_RENDERBUFFER, gbuffer->diffuseBuffer );

  glBindRenderbuffer( GL_RENDERBUFFER, gbuffer->depthBuffer );
  glRenderbufferStorageMultisample( GL_RENDERBUFFER, gbuffer->msaa,
                                    GL_DEPTH_COMPONENT32F, gbuffer->width,
                                    gbuffer->height );
  glFramebufferRenderbuffer( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                             GL_RENDERBUFFER, gbuffer->depthBuffer );

  // TODO: Try and use a better colour encoding scheme.
  glBindRenderbuffer( GL_RENDERBUFFER, gbuffer->positionsBuffer );
  glRenderbufferStorageMultisample( GL_RENDERBUFFER, gbuffer->msaa, GL_RGB32F,
                                    gbuffer->width, gbuffer->height );
  glFramebufferRenderbuffer( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1,
                             GL_RENDERBUFFER, gbuffer->positionsBuffer );

  glBindRenderbuffer( GL_RENDERBUFFER, gbuffer->normalsBuffer );
  glRenderbufferStorageMultisample( GL_RENDERBUFFER, gbuffer->msaa, GL_RGB16F,
                                    gbuffer->width, gbuffer->height );
  glFramebufferRenderbuffer( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2,
                             GL_RENDERBUFFER, gbuffer->normalsBuffer );

  glGenTextures( 1, &gbuffer->diffuseTexture );
  glBindTexture( GL_TEXTURE_2D_MULTISAMPLE, gbuffer->diffuseTexture );
  glTexImage2DMultisample( GL_TEXTURE_2D_MULTISAMPLE, gbuffer->msaa, GL_RGB,
                          gbuffer->width, gbuffer->height, GL_TRUE );
  glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                          GL_TEXTURE_2D_MULTISAMPLE, gbuffer->diffuseTexture,
                          0 );

  glGenTextures( 1, &gbuffer->depthTexture );
  glBindTexture( GL_TEXTURE_2D_MULTISAMPLE, gbuffer->depthTexture );
  glTexImage2DMultisample( GL_TEXTURE_2D_MULTISAMPLE, gbuffer->msaa,
                          GL_DEPTH_COMPONENT32F, gbuffer->width,
                          gbuffer->height, GL_TRUE );
  glFramebufferTexture2D( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                          GL_TEXTURE_2D_MULTISAMPLE, gbuffer->depthTexture, 0 );

  glGenTextures( 1, &gbuffer->positionsTexture );
  glBindTexture( GL_TEXTURE_2D_MULTISAMPLE, gbuffer->positionsTexture );
  glTexImage2DMultisample( GL_TEXTURE_2D_MULTISAMPLE, gbuffer->msaa, GL_RGB32F,
                          gbuffer->width, gbuffer->height, GL_TRUE );
  glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1,
                          GL_TEXTURE_2D_MULTISAMPLE, gbuffer->positionsTexture,
                          0 );

  glGenTextures( 1, &gbuffer->normalsTexture );
  glBindTexture( GL_TEXTURE_2D_MULTISAMPLE, gbuffer->normalsTexture );
  glTexImage2DMultisample( GL_TEXTURE_2D_MULTISAMPLE, gbuffer->msaa, GL_RGB16F,
                          gbuffer->width, gbuffer->height, GL_TRUE );
  glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2,
                          GL_TEXTURE_2D_MULTISAMPLE, gbuffer->normalsTexture,
                          0 );

  glBindFramebuffer( GL_FRAMEBUFFER, 0 );

  gbuffer->renderTargets[0] = GL_COLOR_ATTACHMENT0;
  gbuffer->renderTargets[1] = GL_COLOR_ATTACHMENT1;
  gbuffer->renderTargets[2] = GL_COLOR_ATTACHMENT2;
}

void DeinitializeGBuffer( GBuffer *gbuffer )
{
	if ( gbuffer->fbo )
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		glDeleteTextures(1, &gbuffer->diffuseTexture);
		glDeleteTextures(1, &gbuffer->depthTexture);
		glDeleteTextures(1, &gbuffer->positionsTexture);
		glDeleteTextures(1, &gbuffer->normalsTexture);

		glDeleteFramebuffers(1, &gbuffer->fbo);
		glDeleteRenderbuffers(1, &gbuffer->diffuseBuffer);
		glDeleteRenderbuffers(1, &gbuffer->depthBuffer);
		glDeleteRenderbuffers(1, &gbuffer->positionsBuffer);
		glDeleteRenderbuffers(1, &gbuffer->normalsBuffer);
		gbuffer->fbo = 0;
	}
}

void DisplayGBuffer( GBuffer *gbuffer, OpenGLStaticMesh fullscreenQuad,
                     const ShadowMap &shadowMap )
{
  auto gbufferShader = &gbuffer->shader;
  gbuffer->outputType = RENDER_OUTPUT_SHADED;

  if ( !gbufferShader->program )
  {
    return;
  }

  glUseProgram( gbufferShader->program );
  glActiveTexture( GL_TEXTURE0 );
  glBindTexture( GL_TEXTURE_2D_MULTISAMPLE, gbuffer->diffuseTexture );
  glUniform1i( gbufferShader->diffuseTextureLocation, 0 );
  glActiveTexture( GL_TEXTURE1 );
  glBindTexture( GL_TEXTURE_2D_MULTISAMPLE, gbuffer->positionsTexture );
  glUniform1i( gbufferShader->positionsTextureLocation, 1 );
  glActiveTexture( GL_TEXTURE2 );
  glBindTexture( GL_TEXTURE_2D_MULTISAMPLE, gbuffer->normalsTexture );
  glUniform1i( gbufferShader->normalsTextureLocation, 2 );
  glActiveTexture( GL_TEXTURE3 );
  glBindTexture( GL_TEXTURE_2D_MULTISAMPLE, gbuffer->depthTexture );
  glUniform1i( gbufferShader->depthTextureLocation, 3 );
  glUniform1i( gbufferShader->outputTypeLocation, gbuffer->outputType );
  glUniform1i( gbufferShader->numSamplesLocation, gbuffer->msaa );
  glActiveTexture( GL_TEXTURE4 );
  glBindTexture( GL_TEXTURE_2D, shadowMap.depthTexture );
  glUniform1i( gbufferShader->shadowTextureLocation, 4 );
  glm::mat4 shadowViewProjection =
    shadowMap.projectionMatrix * shadowMap.viewMatrix;
  glUniformMatrix4fv( gbufferShader->shadowViewProjectionLocation, 1, GL_FALSE,
                      glm::value_ptr( shadowViewProjection ) );
  glDisable( GL_DEPTH_TEST );
  OpenGLDrawStaticMesh( fullscreenQuad );
}

void InitializeShadowMap( ShadowMap *shadowMap )
{
  shadowMap->width = 2048;
  shadowMap->height = 2048;
  glGenFramebuffers( 1, &shadowMap->fbo );
  glGenRenderbuffers( 1, &shadowMap->depthBuffer );

  glBindFramebuffer( GL_FRAMEBUFFER, shadowMap->fbo );

  glBindRenderbuffer( GL_RENDERBUFFER, shadowMap->depthBuffer );
  glRenderbufferStorage( GL_RENDERBUFFER, GL_DEPTH_COMPONENT16,
                         shadowMap->width, shadowMap->height );
  glFramebufferRenderbuffer( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                             GL_RENDERBUFFER, shadowMap->depthBuffer );
  // Create depth texture.
  glGenTextures(1, &shadowMap->depthTexture);
  glBindTexture( GL_TEXTURE_2D, shadowMap->depthTexture);
  glTexImage2D( GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, shadowMap->width,
                shadowMap->height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0 );
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE,
                   GL_COMPARE_REF_TO_TEXTURE );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL );
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D,
    shadowMap->depthTexture, 0);

  glBindFramebuffer( GL_FRAMEBUFFER, 0 );
  shadowMap->renderTarget = GL_COLOR_ATTACHMENT0;

  float scale = 60.0f;
  shadowMap->projectionMatrix =
    glm::ortho( -scale, scale, -scale, scale, -scale, scale );
}

Shader LoadShader( const char *vertexPath, const char *fragmentPath )
{
  Shader result = {};
  ReadFileResult vertexFileData = ReadEntireFile( vertexPath );
  ReadFileResult fragmentFileData = ReadEntireFile( fragmentPath );
  if ( vertexFileData.memory && fragmentFileData.memory )
  {
    uint32_t program = OpenGLCreateShader(
      (const char *)vertexFileData.memory, vertexFileData.size,
      (const char *)fragmentFileData.memory, fragmentFileData.size,
      PrintError );

    FreeFileMemory( vertexFileData.memory );
    FreeFileMemory( fragmentFileData.memory );

    if ( program )
    {
      result = GetUniformLocations( program );
    }
    else
    {
      OpenGLDeleteShader( program );
    }
  }
  return result;
}
