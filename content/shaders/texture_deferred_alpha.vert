#version 330

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;
layout(location = 2) in vec2 vertexTextureCoordinate;

uniform mat4 modelMatrix = mat4(1);
uniform mat4 combinedMatrix;

out vec3 normal;
out vec3 position;
out vec2 textureCoordinate;

void main()
{
	textureCoordinate = vertexTextureCoordinate;
	position = vec3( modelMatrix * vec4( vertexPosition, 1.0 ) );
	normal = vec3( transpose( inverse( modelMatrix ) ) *
      vec4( vertexNormal,0.0 ) );
  normal = normalize( normal );
	gl_Position = combinedMatrix * vec4( vertexPosition, 1.0 );
}
