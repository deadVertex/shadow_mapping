#include <cstdio>

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/compatibility.hpp>

#include "opengl_utils.h"
#include "render.h"
#include "csg.h"
#include "utils.h"

static bool run = true;
static glm::mat4 projectionMatrix, viewMatrix;
static bool useFirstPersonCamera = true;
static bool enableMouseLook = true;
static int windowWidth, windowHeight;
static void WindowResizeCallback( GLFWwindow *window, int width, int height )
{
  projectionMatrix = glm::perspective( glm::radians( 80.0f ),
      (float)width / (float)height, 0.1f, 100.0f );
  windowWidth = width;
  windowHeight = height;
}

static void UpdateInputMode( GLFWwindow *window, bool useCursor )
{
  if ( useCursor )
  {
    glfwSetInputMode( window, GLFW_CURSOR, GLFW_CURSOR_DISABLED );
  }
  else
  {
    glfwSetInputMode( window, GLFW_CURSOR, GLFW_CURSOR_NORMAL );
  }
}

enum
{
  KEY_W = 1,
  KEY_S = 2,
  KEY_A = 4,
  KEY_D = 8
};

uint8_t keyStates = 0;
static void KeyCallback( GLFWwindow* window, int key, int scancode, int action,
                         int mods )
{
  if ( action == GLFW_PRESS )
  {
    switch ( key )
    {
    case GLFW_KEY_W:
      keyStates |= KEY_W;
      break;
    case GLFW_KEY_S:
      keyStates |= KEY_S;
      break;
    case GLFW_KEY_A:
      keyStates |= KEY_A;
      break;
    case GLFW_KEY_D:
      keyStates |= KEY_D;
      break;
    case GLFW_KEY_TAB:
      useFirstPersonCamera = !useFirstPersonCamera;
      UpdateInputMode( window, useFirstPersonCamera );
      break;
    case GLFW_KEY_SPACE:
    {
      if ( useFirstPersonCamera )
      {
        enableMouseLook = !enableMouseLook;
        UpdateInputMode( window, enableMouseLook );
      }
      break;
    }
    case GLFW_KEY_ESCAPE:
      run = false;
      break;
    default:
      break;
    }
  }
  else if ( action == GLFW_RELEASE )
  {
    switch ( key )
    {
    case GLFW_KEY_W:
      keyStates &= ~KEY_W;
      break;
    case GLFW_KEY_S:
      keyStates &= ~KEY_S;
      break;
    case GLFW_KEY_A:
      keyStates &= ~KEY_A;
      break;
    case GLFW_KEY_D:
      keyStates &= ~KEY_D;
      break;
    default:
      break;
    }
  }
}

struct Camera
{
  glm::vec3 position, orientation;
  glm::vec3 oldPosition, oldOrientation;
};

static void UpdateCamera( Camera* camera, glm::vec2 rotation,
                          glm::vec3 velocity, float dt )
{
  camera->oldPosition = camera->position;
  camera->oldOrientation = camera->orientation;

  camera->orientation += glm::vec3( rotation, 0 ) * dt;
  camera->orientation.x = glm::clamp(
    camera->orientation.x, -glm::half_pi<float>(), glm::half_pi<float>() );

  glm::mat4 rotationMatrix;
  rotationMatrix =
    glm::rotate( rotationMatrix, camera->orientation.x, glm::vec3{1, 0, 0} );
  rotationMatrix =
    glm::rotate( rotationMatrix, camera->orientation.y, glm::vec3{0, 1, 0} );

  rotationMatrix = glm::inverse( rotationMatrix );
  glm::vec3 forward = glm::vec3( rotationMatrix * glm::vec4{0, 0, 1, 0} );
  forward = glm::normalize( forward );

  glm::vec3 right = glm::vec3( rotationMatrix * glm::vec4{1, 0, 0, 0} );
  right = glm::normalize( right );

  glm::vec3 up = glm::vec3( rotationMatrix * glm::vec4{0, 1, 0, 0} );
  up = glm::normalize( up );

  camera->position += right * velocity.x * dt;
  camera->position += forward * velocity.z * dt;
}

static glm::mat4 CreateViewMatrix( Camera *camera, float interp )
{
  glm::vec3 position =
    glm::lerp( camera->oldPosition, camera->position, interp );
  glm::vec3 orientation =
    glm::lerp( camera->oldOrientation, camera->orientation, interp );

  glm::mat4 viewMatrix;
  viewMatrix =
    glm::rotate( viewMatrix, orientation.x, glm::vec3{1, 0, 0} );
  viewMatrix =
    glm::rotate( viewMatrix, orientation.y, glm::vec3{0, 1, 0} );
  viewMatrix = glm::translate( viewMatrix, -position );
  return viewMatrix;
}

struct OrbitCamera
{
  float distance;
  glm::vec2 oldOrientation, orientation, rotation;
  glm::vec3 centre;
};

static void UpdateCamera( OrbitCamera *camera, float dt )
{
  camera->oldOrientation = camera->orientation;

  camera->orientation += camera->rotation * dt;
  camera->orientation.x = glm::clamp(
    camera->orientation.x, -glm::half_pi<float>(), glm::half_pi<float>() );
}
static glm::mat4 CreateViewMatrix( OrbitCamera *camera, float interp )
{
  glm::vec2 rotation =
    glm::lerp( camera->oldOrientation, camera->orientation, interp );

  glm::mat4 viewMatrix( 1.0f );
  viewMatrix =
    glm::translate( viewMatrix, -glm::vec3( 0, 0, camera->distance ) );
  viewMatrix =
    glm::rotate( viewMatrix, rotation.x, glm::vec3( 1.0f, 0.0f, 0.0f ) );
  viewMatrix =
    glm::rotate( viewMatrix, rotation.y, glm::vec3( 0.0f, 1.0f, 0.0f ) );
  viewMatrix = glm::translate( viewMatrix, camera->centre );
  return viewMatrix;
}

void APIENTRY OpenGLReportErrorMessage( GLenum source, GLenum type, GLuint id,
                                        GLenum severity, GLsizei length,
                                        const GLchar *message,
                                        void *userParam )
{
  const char *typeStr = nullptr;
  switch ( type )
  {
    case GL_DEBUG_TYPE_ERROR:
      typeStr = "ERROR";
      break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
      typeStr = "DEPRECATED_BEHAVIOR";
      break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
      typeStr = "UNDEFINED_BEHAVIOR";
      break;
    case GL_DEBUG_TYPE_PORTABILITY:
      typeStr = "PORTABILITY";
      break;
    case GL_DEBUG_TYPE_PERFORMANCE:
      typeStr = "PERFORMANCE";
      break;
    case GL_DEBUG_TYPE_OTHER:
      typeStr = "OTHER";
      break;
    default:
      typeStr = "";
      break;
  }

  const char *severityStr = nullptr;
  switch ( severity )
  {
    case GL_DEBUG_SEVERITY_LOW:
      severityStr = "LOW";
      break;
    case GL_DEBUG_SEVERITY_MEDIUM:
      severityStr = "MEDIUM";
      break;
    case GL_DEBUG_SEVERITY_HIGH:
      severityStr = "HIGH";
      break;
    default:
      severityStr = "";
      break;
  }

  fprintf( stdout, "OPENGL|%s:%s:%s\n", typeStr, severityStr, message );
}

struct Scene
{
  Shader colourOnlyShader, vertexColourOnlyShader, colourDeferred,
    textureDeferred, textureDeferredStencil, decalShader;
  GBuffer gbuffer;
  OpenGLStaticMesh fullscreenQuad, wireframeCube, axisMesh, monkeyMesh,
    groundPlaneMesh, cubeMesh, levelGeometryMesh;
  uint32_t devTileTexture, fenceTexture, bulletHoleTexture;
  ShadowMap shadowMap;
  CsgSystem *csgSystem;
};

internal CsgShape* CreateMap2( CsgSystem *csgSystem )
{
  auto outer = CsgCreateBox( csgSystem, glm::vec3{-0.4, -0.4, -0.4},
                             glm::vec3{20.4, 3, 20.4} );
  auto inner =
    CsgCreateBox( csgSystem, glm::vec3{-0, 0, -0}, glm::vec3{20, 3, 20} );

  auto col1 =
    CsgCreateBox( csgSystem, glm::vec3{4, 0, 4}, glm::vec3{8, 3, 8} );

  auto col2 =
    CsgCreateBox( csgSystem, glm::vec3{12, 0, 4}, glm::vec3{16, 3, 8} );

  auto col3 =
    CsgCreateBox( csgSystem, glm::vec3{4, 0, 12}, glm::vec3{8, 3, 16} );

  auto col4 =
    CsgCreateBox( csgSystem, glm::vec3{12, 0, 12}, glm::vec3{16, 3, 16} );

  auto level = CsgSubtract( csgSystem, outer, inner );
  level = CsgUnion( csgSystem, level, col1 );
  level = CsgUnion( csgSystem, level, col2 );
  level = CsgUnion( csgSystem, level, col3 );
  level = CsgUnion( csgSystem, level, col4 );
  CsgTranslate( level, glm::vec3{ -2, 0, -2 } );
  return level;
}

internal void GenerateLevel( Scene *scene )
{
  scene->csgSystem = CreateCsgSystem( 1024, 0xFFFF );
  auto level = CreateMap2( scene->csgSystem );
  PolygonVertex vertices[1024];
  uint32_t numVertices =
    CsgGenerateTriangeMeshData( vertices, 1024, level );

  OpenGLVertexAttribute attribs[3];
  attribs[0].index = VERTEX_ATTRIBUTE_POSITION;
  attribs[0].numComponents = 3;
  attribs[0].componentType = GL_FLOAT;
  attribs[0].normalized = GL_FALSE;
  attribs[0].offset = 0;
  attribs[1].index = VERTEX_ATTRIBUTE_NORMAL;
  attribs[1].numComponents = 3;
  attribs[1].componentType = GL_FLOAT;
  attribs[1].normalized = GL_FALSE;
  attribs[1].offset = offsetof( PolygonVertex, normal );
  attribs[2].index = VERTEX_ATTRIBUTE_TEXTURE_COORDINATE;
  attribs[2].numComponents = 2;
  attribs[2].componentType = GL_FLOAT;
  attribs[2].normalized = GL_FALSE;
  attribs[2].offset = offsetof( PolygonVertex, textureCoordinate );
  scene->levelGeometryMesh =
    OpenGLCreateStaticMesh( vertices, numVertices, NULL, 0,
                            sizeof( PolygonVertex ), attribs, 3, GL_TRIANGLES );
}

internal void LoadContent( Scene *scene )
{
  scene->colourOnlyShader = LoadShader( "../content/shaders/colour_only.msh" );
  scene->vertexColourOnlyShader =
    LoadShader( "../content/shaders/vertex_colour_only.msh" );
  scene->gbuffer.shader = LoadShader( "../content/shaders/gbuffer.vert",
                                      "../content/shaders/gbuffer.frag" );
  scene->colourDeferred =
    LoadShader( "../content/shaders/colour_deferred.msh" );
  scene->textureDeferred =
    LoadShader( "../content/shaders/texture_deferred.msh" );
  scene->textureDeferredStencil =
    LoadShader( "../content/shaders/texture_deferred_alpha.vert",
                "../content/shaders/texture_deferred_alpha.frag" );
  scene->decalShader = LoadShader( "../content/shaders/decal.vert",
                                   "../content/shaders/decal.frag" );

  InitializeGBuffer( &scene->gbuffer, windowWidth, windowHeight );
  scene->fullscreenQuad = CreateFullscreenQuadMesh();
  scene->wireframeCube = CreateWireframeCube();
  scene->axisMesh = CreateAxisMesh();
  scene->monkeyMesh = LoadMesh( "../content/meshes/monkey.mme" );
  scene->groundPlaneMesh = CreatePlaneMesh( 2.0f, 1.0f, 8.0f );
  scene->cubeMesh = CreateCubeMesh();
  scene->devTileTexture = LoadTexture( "../content/textures/dev_tile512.mte" );
  scene->fenceTexture = LoadTexture( "../content/textures/fence.mte" );
  scene->bulletHoleTexture = LoadTexture( "../content/textures/bullethole.mte" );

  InitializeShadowMap( &scene->shadowMap );
  scene->shadowMap.viewMatrix =
    glm::rotate( glm::mat4(), glm::quarter_pi<float>(), glm::vec3{1, 0, 0} );

  GenerateLevel( scene );
}

internal void RenderScene( Scene *scene, const glm::mat4 &viewProjection )
{
  glUseProgram( scene->colourDeferred.program );
  glm::vec4 colour2{0.8, 0.8, 0.8, 1};
  glUniform4fv( scene->colourDeferred.colourLocation, 1,
                glm::value_ptr( colour2 ) );
  glm::mat4 monkeyModelMatrix =
    glm::translate( glm::mat4(), glm::vec3{0, 1, 0} );
  auto monkeyCombined = viewProjection * monkeyModelMatrix;
  glUniformMatrix4fv( scene->colourDeferred.combinedMatrixLocation, 1, GL_FALSE,
                      glm::value_ptr( monkeyCombined ) );
  glUniformMatrix4fv( scene->colourDeferred.modelMatrixLocation, 1, GL_FALSE,
                      glm::value_ptr( monkeyModelMatrix ) );
  OpenGLDrawStaticMesh( scene->monkeyMesh );

  glm::mat4 identity;
  glUseProgram( scene->textureDeferred.program );
  glUniformMatrix4fv( scene->textureDeferred.combinedMatrixLocation, 1,
      GL_FALSE, glm::value_ptr( viewProjection ) );
  glUniformMatrix4fv( scene->textureDeferred.modelMatrixLocation, 1, GL_FALSE,
                      glm::value_ptr( identity ) );
  glActiveTexture( GL_TEXTURE0 );
  glBindTexture( GL_TEXTURE_2D, scene->devTileTexture );
  glUniform1i( scene->textureDeferred.diffuseTextureLocation, 0 );
  OpenGLDrawStaticMesh( scene->levelGeometryMesh );

  glUseProgram( scene->textureDeferredStencil.program );
  glm::mat4 modelMatrix = glm::translate( glm::mat4(), glm::vec3{ 0, 1, 5 } );
  modelMatrix =
    glm::rotate( modelMatrix, glm::half_pi<float>(), glm::vec3{1, 0, 0} );
  auto combined = viewProjection * modelMatrix;
  glUniformMatrix4fv( scene->textureDeferredStencil.combinedMatrixLocation, 1,
      GL_FALSE, glm::value_ptr( combined ) );
  glUniformMatrix4fv( scene->textureDeferredStencil.modelMatrixLocation, 1, GL_FALSE,
                      glm::value_ptr( modelMatrix ) );
  glActiveTexture( GL_TEXTURE0 );
  glBindTexture( GL_TEXTURE_2D, scene->fenceTexture );
  glUniform1i( scene->textureDeferredStencil.diffuseTextureLocation, 0 );
  OpenGLDrawStaticMesh( scene->groundPlaneMesh );
}
internal void RenderDecals( Scene *scene, const glm::mat4 &viewProjection )
{
  //glFramebufferTexture2D( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                          //GL_TEXTURE_2D_MULTISAMPLE, 0, 0 );
  glUseProgram( scene->decalShader.program );
  glm::mat4 decalModelMatrix =
    glm::translate( glm::mat4(), glm::vec3{0.3, 1.2, 0.75} );
  decalModelMatrix = glm::scale( decalModelMatrix, glm::vec3{ 0.2f } );
  auto decalCombined = viewProjection * decalModelMatrix;
  glUniformMatrix4fv( scene->decalShader.combinedMatrixLocation, 1, GL_FALSE,
                      glm::value_ptr( decalCombined ) );
  glUniformMatrix4fv( scene->decalShader.modelMatrixLocation, 1, GL_FALSE,
                      glm::value_ptr( decalModelMatrix ) );
  glActiveTexture( GL_TEXTURE0 );
  glBindTexture( GL_TEXTURE_2D_MULTISAMPLE, scene->gbuffer.depthTexture );
  glUniform1i( scene->decalShader.depthTextureLocation, 0 );
  glUniform1i( scene->decalShader.numSamplesLocation, scene->gbuffer.msaa );
  glUniform2f( scene->decalShader.windowDimensionsLocation, windowWidth,
               windowHeight );
  glm::mat4 invViewProjection = glm::inverse( viewProjection );
  glUniformMatrix4fv( scene->decalShader.invViewProjectionLocation, 1, GL_FALSE,
                      glm::value_ptr( invViewProjection ) );
  glm::mat4 invModelMatrix = glm::inverse( decalModelMatrix );
  glUniformMatrix4fv( scene->decalShader.invModelMatrixLocation, 1, GL_FALSE,
                      glm::value_ptr( invModelMatrix ) );
  glActiveTexture( GL_TEXTURE1 );
  glBindTexture( GL_TEXTURE_2D, scene->bulletHoleTexture );
  glUniform1i( scene->decalShader.diffuseTextureLocation, 1 );
  OpenGLDrawStaticMesh( scene->cubeMesh );
  //glFramebufferTexture2D( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                          //GL_TEXTURE_2D_MULTISAMPLE,
                          //scene->gbuffer.depthTexture, 0 );
}

internal void Render( Scene *scene, const glm::mat4 &viewProjection )
{
  glViewport( 0, 0, windowWidth, windowHeight );
  glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
  glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
  glEnable( GL_DEPTH_TEST );
  glEnable( GL_TEXTURE_2D );
  glEnable( GL_FRAMEBUFFER_SRGB );

  if ( scene->shadowMap.fbo )
  {
    glBindFramebuffer( GL_FRAMEBUFFER, scene->shadowMap.fbo );
    glViewport( 0, 0, scene->shadowMap.width, scene->shadowMap.height );
    glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glEnable( GL_DEPTH_TEST );
    glDrawBuffers( 1, &scene->shadowMap.renderTarget );

    glm::mat4 shadowViewProjection =
      scene->shadowMap.projectionMatrix * scene->shadowMap.viewMatrix;

    RenderScene( scene, shadowViewProjection );
    glBindFramebuffer( GL_FRAMEBUFFER, 0 );
  }
  if ( scene->gbuffer.fbo )
  {
    glBindFramebuffer( GL_FRAMEBUFFER, scene->gbuffer.fbo );
    glViewport( 0, 0, windowWidth, windowHeight );
    glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glActiveTexture( GL_TEXTURE0 );
    glDrawBuffers( 3, scene->gbuffer.renderTargets );

    glm::mat4 shadowViewProjection =
      scene->shadowMap.projectionMatrix * scene->shadowMap.viewMatrix;
    RenderScene( scene, viewProjection );

    // Do decal pass.

    glBindFramebuffer( GL_FRAMEBUFFER, 0 );
    glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glActiveTexture( GL_TEXTURE0 );
    glViewport( 0, 0, windowWidth, windowHeight );
    glEnable( GL_TEXTURE_2D );
    glEnable( GL_BLEND );
    glBlendFunc( GL_ONE, GL_ONE_MINUS_SRC_ALPHA );
    glDisable( GL_DEPTH_TEST );
    DisplayGBuffer( &scene->gbuffer, scene->fullscreenQuad, scene->shadowMap );
    RenderDecals( scene, viewProjection );
  }
}

int main( int argc, char **argv )
{
  if ( !glfwInit() )
  {
    fprintf( stderr, "Failed to initialize GLFW.\n" );
    return -1;
  }
  //glfwWindowHint( GLFW_DOUBLEBUFFER, 1 );
  glfwWindowHint( GLFW_SAMPLES, 0 );
  glfwWindowHint( GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE );
  glfwWindowHint( GLFW_DECORATED, false );
  GLFWwindow *window =
    glfwCreateWindow( 1920, 1080, "Shadow Mapping", NULL, NULL );
  windowWidth = 1920;
  windowHeight = 1080;
  if ( !window )
  {
    glfwTerminate();
    fprintf( stderr, "Failed to create GLFW window.\n" );
    return -1;
  }

  glfwMakeContextCurrent( window );
  glfwSetWindowSizeCallback( window, WindowResizeCallback );
  glfwSetKeyCallback( window, KeyCallback );
  glfwSetInputMode( window, GLFW_CURSOR, GLFW_CURSOR_DISABLED );
  glfwSwapInterval( 0 );

  GLenum err = glewInit();
  if ( err != GLEW_OK )
  {
    fprintf( stderr, "GLEW ERROR: %s\n", glewGetErrorString( err ) );
    return -1;
  }
  fprintf( stdout, "Using GLEW %s.\n", glewGetString( GLEW_VERSION ) );

  glEnable( GL_DEBUG_OUTPUT_SYNCHRONOUS );
  glDebugMessageCallback( OpenGLReportErrorMessage, nullptr );
  GLuint unusedIds = 0;
  glDebugMessageControl( GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0,
                         &unusedIds, GL_TRUE );

  Scene scene;
  LoadContent( &scene );

  double t = 0.0;
  float logicHz = 30.0f;
  float logicTimestep = 1.0f / logicHz;
  double maxFrameTime = 0.25; // Used to prevent the simulation from being
                              // affected by break points.

  double currentTime = glfwGetTime();
  double accumulator = 0.0;

  double prevMouseX = 0.0, prevMouseY = 0.0;
  Camera camera;
  OrbitCamera orbitCamera;
  orbitCamera.distance = 10.0f;
  orbitCamera.rotation = glm::vec2{ 0, 0.1f };
  orbitCamera.orientation = glm::vec2{ 0.5f, 0 };
  useFirstPersonCamera = false;
  while ( run )
  {
    double newTime = glfwGetTime();
    double frameTime = newTime - currentTime;
    if ( frameTime > maxFrameTime )
      frameTime = maxFrameTime;
    currentTime = newTime;

    accumulator += frameTime;

    while ( accumulator >= logicTimestep )
    {
      glfwPollEvents();
      if ( glfwWindowShouldClose( window ) )
      {
        run = false;
      }

      double mousePosX, mousePosY;
      glfwGetCursorPos( window, &mousePosX, &mousePosY );
      double dx = mousePosX - prevMouseX;
      double dy = mousePosY - prevMouseY;
      prevMouseX = mousePosX;
      prevMouseY = mousePosY;
      glm::vec2 rotation{ dy, dx };
      rotation *= 0.03f;

      glm::vec3 velocity;
      float cameraSpeed = 2.0f;
      if ( keyStates & KEY_W )
      {
        velocity.z = -cameraSpeed;
      }
      if ( keyStates & KEY_S )
      {
        velocity.z = cameraSpeed;
      }
      if ( keyStates & KEY_A )
      {
        velocity.x = -cameraSpeed;
      }
      if ( keyStates & KEY_D )
      {
        velocity.x = cameraSpeed;
      }
      if ( useFirstPersonCamera )
      {
        if ( !enableMouseLook )
        {
          rotation = glm::vec2{};
        }
        UpdateCamera( &camera, rotation, velocity, logicTimestep );
      }
      else
      {
        UpdateCamera( &orbitCamera, logicTimestep );
      }
      t += logicTimestep;
      accumulator -= logicTimestep;
    }

    double interp = accumulator / logicTimestep;
    glm::mat4 viewMatrix;
    if ( useFirstPersonCamera )
    {
      viewMatrix = CreateViewMatrix( &camera, interp );
      orbitCamera.centre = -camera.position;
    }
    else
    {
      viewMatrix = CreateViewMatrix( &orbitCamera, interp );
    }
    glm::mat4 viewProjection = projectionMatrix * viewMatrix;
    Render( &scene, viewProjection );
    glfwSwapBuffers( window );
  }
  glfwTerminate();
  return 0;
}
