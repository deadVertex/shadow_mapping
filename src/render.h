#pragma once
#include <cstdint>

#include "opengl_utils.h"

#include <glm/glm.hpp>

struct Shader
{
  uint32_t program;
  int colourLocation, diffuseTextureLocation, positionsTextureLocation,
    normalsTextureLocation, depthTextureLocation, outputTypeLocation,
    modelMatrixLocation, combinedMatrixLocation, numSamplesLocation,
    shadowTextureLocation, shadowViewProjectionLocation,
    windowDimensionsLocation, invViewProjectionLocation, invModelMatrixLocation;
};

struct ShadowMap
{
  uint32_t fbo, depthBuffer, depthTexture, width, height, renderTarget;
  glm::mat4 projectionMatrix, viewMatrix;
};

struct GBuffer
{
  uint32_t fbo, diffuseBuffer, depthBuffer, positionsBuffer, normalsBuffer;
  uint32_t diffuseTexture, depthTexture, positionsTexture, normalsTexture;
  uint32_t renderTargets[3];
  uint8_t outputType, msaa;
  uint32_t width, height;
  Shader shader;
};

extern Shader LoadShader( const char *path );
extern OpenGLStaticMesh LoadMesh( const char *path );
extern uint32_t LoadTexture( const char *path );
extern void InitializeGBuffer( GBuffer *gbuffer, uint32_t width,
                               uint32_t height );
extern void DeinitializeGBuffer( GBuffer *gbuffer );
extern void DisplayGBuffer( GBuffer *gbuffer, OpenGLStaticMesh fullscreenQuad,
                            const ShadowMap &shadowMap );

extern void InitializeShadowMap( ShadowMap *shadowMap );
extern Shader CreateShader( const char *vertex, const char *fragment );
extern Shader LoadShader( const char *vertexPath, const char *fragmentPath );
